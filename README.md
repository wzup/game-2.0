This is a Test Work.

The game imitates a fruit machine.
You choose a fruit in <select>, then press a [Spielen!] button and start to hope that the chosen image will appear.

Technologies used:

NO FRAMEWORKS USED!

True, pure, native JS only.

Node.js as a back-end, <canvas> element to show fruit images.

Only 3 files you are interested:

./public/index.html

./public/data.json

./public/javascript/index.js

To run the program, just clone the repo, cd to "/game-2.0" directory, run "node bin/www" and visit "http://localhost:3000/" in your browser. 

You need to have Node.js instaled.