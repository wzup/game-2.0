

window.addEventListener("load", function(e) {
    console.log("[document.readyState 2] :", document.readyState);
    btn = document.getElementById("btn");
    txtSpan = document.getElementById("text");
    sel = document.getElementById("sel");
    opt = null;
    config = null;
    btn.setAttribute("disabled", "disabled");
    getConfig();
});

// Аякс получить конфиг
function getConfig() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'data.json', true);
    xhr.onreadystatechange = function() {
    if (xhr.readyState != 4) return;
        config = JSON.parse(xhr.responseText);
        btn.onclick = btnOnclick;
        sel.onchange = selOnchange;
    }
    xhr.send(null);
}


function selOnchange(event) {
    btn.removeAttribute("disabled");
    opt = event.target.value;
}


function getRandomImg(conf) {
    return _.sample(conf, 1);
};

function btnOnclick() {
    var c=document.getElementById("canvas");
    var ctx=c.getContext("2d");
    ctx.clearRect(0, 0, c.width, c.height);
    // var img=document.getElementById("img");
    var img=document.createElement("img");
    var imgUrl = getRandomImg(config)[0];
    img.setAttribute("src", imgUrl);
    

    img.onload = function() {
        ctx.drawImage(img, 10, 10);

        if(_.invert(config)[imgUrl] == opt) {
            // console.log("[BINGO] : YOU WON!", $scope.selectedOpt);
            txtSpan.innerHTML = "BINGO! YOU WON!";
            txtSpan.removeAttribute("class");
            txtSpan.setAttribute("class", "win");
        }
        else {
            // console.log("[SORRY] : TRY AGAIN!", $scope.selectedOpt);
            txtSpan.removeAttribute("class");
            txtSpan.setAttribute("class", "wrong");
            txtSpan.innerHTML = "Sorry. Try Again.";
        }        
    
        // Здесь прятать не надо, т.к. если захочет повторить
        // btn.setAttribute("disabled", "disabled");
        img = null;
    };
    btn.removeAttribute("disabled");    
}